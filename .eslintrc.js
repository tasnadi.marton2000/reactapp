const c = {
	OFF: 0,
	WARNING: 1,
	ERROR: 2,
};

module.exports = {
	extends: [
		"eslint:recommended",
		"prettier",
		"plugin:react/recommended",
		"plugin:react-hooks/recommended",
	],
	env: {
		browser: true,
		es6: true,
		node: true,
	},
	plugins: ["prettier", "react", "react-hooks", "import"],
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		sourceType: "module",
	},
	rules: {
		"react/jsx-props-no-spreading": [
			c.WARNING,
			{
				exceptions: ["FormattedMessage"],
			},
		],
		"prettier/prettier": [
			c.WARNING,
			{
				trailingComma: "es5",
				useTabs: true,
			},
		],

		"import/no-default-export": c.ERROR,
		"import/order": [
			c.WARNING,
			{
				groups: [
					["builtin"],
					["external"],
					["internal", "parent", "sibling", "index"],
				],
				"newlines-between": "always",
			},
		],

		"react/no-multi-comp": [c.WARNING, { ignoreStateless: true }],

		"react/no-unused-prop-types": c.OFF,
		"react/prop-types": c.OFF,
		"react/require-default-props": c.OFF,

		"react/jsx-max-depth": [c.WARNING, { max: 7 }],

		"react/display-name": c.OFF,
		"react/jsx-indent": c.OFF,
		"react/require-optimization": c.OFF,
		"react/function-component-definition": c.OFF,
		"react/sort-comp": c.OFF,
		"react/no-set-state": c.OFF,
		"react/jsx-no-literals": c.OFF,
		"react/forbid-component-props": c.OFF,
		"react/jsx-no-bind": c.OFF,

		"react/jsx-filename-extension": [
			c.WARNING,
			{ extensions: [".jsx", ".tsx"] },
		],
	},
	settings: {
		react: {
			version: "detect",
		},
	},

	overrides: [
		{
			files: ["*.js", "*.jsx"],
			parser: "babel-eslint",
		},
		{
			files: ["*.ts", "*.tsx"],
			parser: "@typescript-eslint/parser",
			plugins: ["@typescript-eslint/eslint-plugin"],
			rules: {
				"@typescript-eslint/no-unused-vars": [
					c.ERROR,
					{ argsIgnorePattern: "^_" },
				],
				"no-dupe-class-members": c.OFF,
				"no-unused-vars": c.OFF,
			},
		},
	],
};
