const manageTranslations = require("react-intl-translations-manager").default;
manageTranslations({
	messagesDirectory: "messages",
	translationsDirectory: "src/locales",
	languages: ["hu"],
	jsonOptions: {
		space: 4,
		trailingNewline: true,
	},
});
