export enum EnrollmentEnum {
	Closed = 0,
	Open = 1,
	ForRecurringUsers = 2,
}
export enum SettingsKey {
	Enrollment = "Enrollment",
}
