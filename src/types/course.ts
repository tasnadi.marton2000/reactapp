import { CourseType } from "./courseType";

// it is used also as a detaliated dto
export type Course = {
	id: number;
	type: CourseType;
	day: string;
	startHour: string;
	endHour: string;
	startDate: string;
	endDate: string;
	nrOfEnrolledPersons: number;
	participationLimit: number;
};

export type CourseCreationDto = {
	type: string;
	day: number;
	startHour: string;
	endHour: string;
	startDate: string;
	endDate: string;
	participationLimit: number;
	extraPlaces: number;
};

export type CourseReducedDto = {
	id: number;
	type: string;
	day: number;
	startHour: string;
	endHour: string;
	nrOfEnrolledPersons: number;
	participationLimit: number;
};
