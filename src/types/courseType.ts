// every dto contains full information about CourseType
// this type can be used like an incoming as well as an outgoing dto
export type CourseType = {
	name: string;
};
