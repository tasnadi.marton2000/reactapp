import { CourseReducedDto } from "./course";

export type Training = {
	id: number;
	date: string;
	extraPlaces: number;
	course: CourseReducedDto;
	isCancelled: boolean;
};

export type TrainingDto = {
	id: number;
	date: string;
	course: CourseReducedDto;
	isCancelled: boolean;
};

export type CancelTrainingDto = {
	trainingId: number;
	reason: string;
};
