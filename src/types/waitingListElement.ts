export type WaitingListElementDto = {
	courseId: number;
	personId: number;
	notes: string;
};
