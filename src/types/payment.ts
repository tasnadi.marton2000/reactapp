import { Course, CourseReducedDto, Person, PersonReducedDto } from ".";

export type Payment = {
	person: Person;
	course: Course;
	month: number;
	isPaid: boolean;
};

export type PaymentSkeleton = {
	month: number;
	isPaid: boolean;
};

export type PaymentDto = {
	id: number;
	person: PersonReducedDto;
	course: CourseReducedDto;
	months: PaymentSkeleton[];
};

export type PaymentSetterDto = {
	personId: number;
	courseId: number;
	months: number[];
};
