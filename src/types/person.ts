export type Person = {
	id: number;
	name: string;
	birthDate: string;
	medicalNotes?: string;
	userId: number;
};

export type PersonCreationDto = {
	name: string;
	birthDate: string;
	medicalNotes?: string;
};

export type PersonEditDto = {
	name: string;
	birthDate: string;
};

export type PersonDto = {
	id: number;
	userId: string;
	name: string;
	birthDate: string;
	creatorName: string;
	creatorEmail: string;
	creatorTel: string;
	medicalNotes?: string;
};

export type PersonReducedDto = {
	id: number;
	name: string;
	birthDate: string;
};
