import { Role } from ".";

export type UserDto = {
	id?: string;
	userName?: string;
	role?: Role;
	loginStatus: boolean;
};
