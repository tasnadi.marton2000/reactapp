export type RegistrationDto = {
	name: string;
	email: string;
	password: string;
	passwordConfirmation: string;
	addressTel: string;
};
