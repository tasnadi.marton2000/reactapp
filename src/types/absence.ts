import { Training } from "./training";

export type Absence = {
	id: number;
	personId: number;
	absenceTraining: Training;
	recuperationTraining: Training;
};

export type AbsenceCreationDto = {
	personId: number;
	trainingId: number;
};

export type AbsenceRecuperationCreationDto = {
	recuperationTrainingId: number;
};
