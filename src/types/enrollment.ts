import { PersonDto } from ".";

export type Enrollment = {
	id: number;
	userId: string;
	personId: number;
	courseId: number;
	notes: string;
	accepted: boolean;
};

export type EnrollmentCreationDto = {
	courseId: number;
	personId: number;
	notes: string;
};

export type EnrollmentDto = {
	id: number;
	courseId: number;
	person: PersonDto;
	notes: string;
	accepted: boolean;
};
